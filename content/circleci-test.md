Title: CircleCI Test
Date: 2018-08-06 12:00

Using a simple project to try CircleCI. Though the project is simple by itself,
its build environment isn't trivial and should be good for testing.

Add a line to trigger a build.
